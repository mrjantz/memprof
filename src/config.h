#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "simple_config.h"

#include <stdio.h>
#include <errno.h>
#include <fstream>

typedef struct {
    struct scfg *_scfg;

    int         profile_period_ms;
    int         profile_overflow_thresh;
    int         print_profile_val;
    int         region_shift;
    int         nr_procs;
    float       alpha;
    float       print_region_limit;
    float       print_misses_limit;

    const char *profile_file_string;

} md_config;

extern md_config config;
extern std::ofstream profile_file;

int  init_config(int argc, char **argv);
void print_config(void);

#endif
