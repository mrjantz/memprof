#include "config.h"

#define SIMPLE_CONFIG_IMPL
#include "simple_config.h"

#include <limits.h>
#include <iostream>

md_config config;
std::ofstream profile_file;

static int between_zero_one_f(struct scfg *cfg, float val) {
    scfg_validate_set_err(cfg, "value must be in the range [0.0, 1.0]");
    return val >= 0.0 && val <= 1.0;
}

int init_config(int argc, char **argv) {
    int status;

    status = 0;

    config._scfg = scfg_make();

    scfg_add_int(            config._scfg, "profile-period-ms",            &config.profile_period_ms);
        scfg_default_int(    config._scfg, "profile-period-ms",            1000);
    scfg_add_int(            config._scfg, "profile-overflow-thresh",      &config.profile_overflow_thresh);
        scfg_default_int(    config._scfg, "profile-overflow-thresh",      1024);
    scfg_add_string(         config._scfg, "profile-file-string",          &config.profile_file_string);
        scfg_default_string( config._scfg, "profile-file-string",          "profile.txt");

    scfg_add_int(            config._scfg, "nr-procs",                     &config.nr_procs);
        scfg_default_int(    config._scfg, "nr-procs",                     0);
    scfg_add_int(            config._scfg, "print-profile-val",            &config.print_profile_val);
        scfg_default_int(    config._scfg, "print-profile-val",            50);
    scfg_add_int(            config._scfg, "region-shift",                 &config.region_shift);
        scfg_default_int(    config._scfg, "region-shift",                 12);
    scfg_add_float(          config._scfg, "alpha",                        &config.alpha);
      scfg_default_float(    config._scfg, "alpha",                        0);
      scfg_validate_float(   config._scfg, "alpha",                        between_zero_one_f);
    scfg_add_float(          config._scfg, "print-region-limit",           &config.print_region_limit);
      scfg_default_float(    config._scfg, "print-region-limit",           1.0);
      scfg_validate_float(   config._scfg, "print-region-limit",           between_zero_one_f);
    scfg_add_float(          config._scfg, "print-misses-limit",           &config.print_misses_limit);
      scfg_default_float(    config._scfg, "print-misses-limit",           1.0);
      scfg_validate_float(   config._scfg, "print-misses-limit",           between_zero_one_f);

    if (argc == 2) {
        status = scfg_parse(config._scfg, argv[1]);
    } else {
        printf("usage: %s CONFIG\n", argv[0]);
        return 1;
    }

    if (status != SCFG_ERR_NONE) {
        printf("%s\n", scfg_err_msg(config._scfg));
    }

    profile_file.open(config.profile_file_string, std::ios::out);
    if (!(profile_file.is_open())) {
        std::cerr << "error opening profile_file: "
                  << config.profile_file_string
                  << std::endl;
        exit(-EINVAL);
    }

    return status;
}

void print_config(void) {
    printf("============================ CURRENT CONFIG ============================\n");
    printf("%-30s %d\n",    "profile-period-ms",            config.profile_period_ms);
    printf("%-30s %d\n",    "profile-overflow-thresh",      config.profile_overflow_thresh);
    printf("%-30s %d\n",    "nr-procs",                     config.nr_procs);
    printf("%-30s %d\n",    "region-shift",                 config.region_shift);
    printf("========================================================================\n\n");
}
