#include "config.h"
#include "profile.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int main(int argc, char **argv) {
    printf("memprof -- memory profiler written in BPF\n\n");

    if (init_config(argc, argv)  != 0) { return 1; }
        print_config();

    if (init_profiling()         != 0) { return 1; }
    if (start_profiling_thread() != 0) { return 1; }

    printf(":: Everything is initialized.\n\n");

    for (;;) {
        fflush(stdout);
        sleep(1);
    }

    return 0;
}
