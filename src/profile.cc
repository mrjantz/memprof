#include <linux/perf_event.h>
#include <perfmon/pfmlib_perf_event.h>
#include <fcntl.h>
#include <unistd.h>
#include <iomanip>
#include <iostream>
#include <string>
#include <algorithm>
#include <sys/sysinfo.h>

#include "config.h"
#include "common.h"
#include "BPF.h"
#include <linux/bpf.h>

#define LINE_SIZE 512
#define MAX_PROCS 4
#define MEMPROF_REGION_SHIFT 21
#define MAX_REGIONS 20480
#define DDR_MISS_EVENT_STR "MEM_LOAD_UOPS_LLC_MISS_RETIRED.LOCAL_DRAM"
#define PMM_MISS_EVENT_STR "MEM_LOAD_UOPS_RETIRED.LOCAL_PMM"

const std::string BPF_PROGRAM = R"(
#include <linux/ptrace.h>
#include <linux/perf_event.h>
#include <uapi/linux/bpf_perf_event.h>

typedef struct __attribute__ ((__packed__)) {
    u32 pid;
    u32 tid;
    u64 time;
    u64 virt_addr;
    u64 phys_addr;
} event_sample;

#define MAX_PROCS 4
#define MAX_REGIONS 20480
#define MEMPROF_REGION_SHIFT 21

BPF_ARRAY(debug_control, int, 1);
BPF_HASH(bpf_procs, int, int, MAX_PROCS);
BPF_PERCPU_HASH(ddr_region_map_0, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(ddr_region_map_1, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(ddr_region_map_2, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(ddr_region_map_3, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(pmm_region_map_0, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(pmm_region_map_1, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(pmm_region_map_2, u64, u64, MAX_REGIONS);
BPF_PERCPU_HASH(pmm_region_map_3, u64, u64, MAX_REGIONS);

BPF_HASH_OF_MAPS(ddr_misses, "ddr_region_map_0", MAX_PROCS);
BPF_HASH_OF_MAPS(pmm_misses, "pmm_region_map_0", MAX_PROCS);

static u64* region_map_lookup_or_try_init( int *map, u64 *key ) {
    u64 *val;
    u64  zero;
    long err;

    zero = 0;
    val = bpf_map_lookup_elem(map, key);
    if (val) {
        return val;
    }

    err = bpf_map_update_elem(map, key, &zero, BPF_NOEXIST);
    if (err && err != -EEXIST) {
        return NULL;
    }

    return bpf_map_lookup_elem(map, key);
}

static int record_llc_miss( int *map, struct bpf_perf_event_data *ctx ) {
    u64  key;
    u64 *val;

    if (!ctx) {
        return 0;
    }

    /* MRJ: I think this is the virtual address -- but have not yet confirmed.
     * I also do not know how to get other relevant information (pid, physical
     * address, sample time, etc.) with this API
     */
    key = (ctx->addr >> MEMPROF_REGION_SHIFT);
    val = region_map_lookup_or_try_init(map, &key);
    if (val) {
        (*val) += 1;
    }

    return 0;

#if 0
    struct bpf_perf_event_data_kern *kctx;
    struct perf_sample_data data;
    u64 pid;
    u64 *ptr;
    u64 ctx_addr;
    int *cnt;
    int  zero;

    zero = 0;
    cnt = debug_control.lookup(&zero);
    if (!cnt) {
        return 0;
    }
    (*cnt) -= 1;

    if ((*cnt) > 0) {
        ctx_addr = ctx->addr;
        bpf_trace_printk("addr:   %lx\n", ctx_addr);
 
        kctx = (struct bpf_perf_event_data_kern *)ctx;
        if (kctx) {
            bpf_probe_read(&data, sizeof(struct perf_sample_data), kctx->data);
            bpf_trace_printk("kern:   %lx\n", data.addr);
        }
    }
#endif

#if 0
    int *cnt;
    int  zero;

    zero = 0;
    cnt = debug_control.lookup(&zero);
    if (!cnt) {
        return 0;
    }
    (*cnt) -= 1;

    if ((*cnt) > 0) {
        bpf_trace_printk("addr: %lx\n", ctx->addr);
    }
#endif
}

int on_ddr_miss(struct bpf_perf_event_data *ctx) {
    int  key;
    int *memprof_id;
    int *region_map;

    key = ( ( bpf_get_current_pid_tgid() >> 32 ) );
    memprof_id = bpf_procs.lookup(&key);
    if (!memprof_id) {
        return 0;
    }

    region_map = ddr_misses.lookup(memprof_id);
    if (!region_map) {
        return 0;
    }

    record_llc_miss(region_map, ctx);
    return 0;
}

int on_pmm_miss(struct bpf_perf_event_data *ctx) {
    int  key;
    int *memprof_id;
    int *region_map;

    key = ( ( bpf_get_current_pid_tgid() >> 32 ) );
    memprof_id = bpf_procs.lookup(&key);
    if (!memprof_id) {
        return 0;
    }

    region_map = pmm_misses.lookup(memprof_id);
    if (!region_map) {
        return 0;
    }

    record_llc_miss(region_map, ctx);
    return 0;
}
)";

enum llc_miss_event {
    DDR_MISS_EVENT,
    PMM_MISS_EVENT,
    NR_MISS_EVENTS
};

typedef struct {
    u64 llc_misses_val[NR_MISS_EVENTS];
    u64 llc_misses_tot[NR_MISS_EVENTS];
    float llc_misses_scaled[NR_MISS_EVENTS];
} region_info_t;

static u64 start_timestamp_realtime;
static u64 start_timestamp_monotonic;
static u64 start_ms;
static pthread_t profiling_pthread;
static int profile_print_cnt;
std::map<pid_t, std::map<u64, region_info_t>> profile;

ebpf::BPF bpf;
std::vector<int> free_memprof_ids;
std::map<pid_t, int> proc_to_memprof_id;
std::map<int, pid_t> memprof_id_to_proc;


static int take_memprof_id() {
    int id;
    
    id = free_memprof_ids.back();
    free_memprof_ids.pop_back();

    return id;
}

static void put_memprof_id(int id) {
    free_memprof_ids.push_back(id);
}

region_info_t make_region_info() {
    region_info_t r;
    r.llc_misses_val[DDR_MISS_EVENT]    = ((u64)0);
    r.llc_misses_val[PMM_MISS_EVENT]    = ((u64)0);
    r.llc_misses_tot[DDR_MISS_EVENT]    = ((u64)0);
    r.llc_misses_tot[PMM_MISS_EVENT]    = ((u64)0);
    r.llc_misses_scaled[DDR_MISS_EVENT] = 0.0;
    r.llc_misses_scaled[PMM_MISS_EVENT] = 0.0;
    return r;
}

region_info_t& get_region_info(pid_t pid, u64 addr) {
    if ( profile.find(pid) == profile.end() ) {
        std::cerr << "could not find pid in profile: PID: "
                  << pid << " addr: " << addr << std::endl;
        exit(-EINVAL);
    }
    if ( profile[pid].find(addr) == profile[pid].end() ) {
        profile[pid][addr] = make_region_info();
    }
    return profile[pid][addr];
}

bool region_info_greater( std::pair<u64, region_info_t>& a, 
                          std::pair<u64, region_info_t>& b ) {
    float a_misses, b_misses;
    
    a_misses = a.second.llc_misses_scaled[0] +
               a.second.llc_misses_scaled[1];
    b_misses = b.second.llc_misses_scaled[0] +
               b.second.llc_misses_scaled[1];

    return (a_misses > b_misses);
}

std::vector<std::pair<u64, region_info_t>> sorted_region_infos (
    std::map<u64, region_info_t> &m ) {

    std::vector<std::pair<u64, region_info_t> > vec;  
    for (auto& it : m) { 
        vec.push_back(it); 
    }
  
    // Sort using comparator function 
    sort(vec.begin(), vec.end(), region_info_greater); 
    return vec;
}


ebpf::BPFHashTable<int, int> get_bpf_procs() {
    auto bpf_procs = bpf.get_hash_table<int, int>("bpf_procs");
    return bpf_procs;
}

ebpf::BPFMapInMapTable get_ddr_misses() {
    auto ddr_misses = bpf.get_map_in_map_table("ddr_misses");
    return ddr_misses;
}

ebpf::BPFMapInMapTable get_pmm_misses() {
    auto pmm_misses = bpf.get_map_in_map_table("pmm_misses");
    return pmm_misses;
}

ebpf::BPFPercpuHashTable<u64, u64> get_region_map(enum llc_miss_event evt,
    int id) {
    char buf[32];

    sprintf(buf, "%s_region_map_%d",
            ( evt == DDR_MISS_EVENT ? "ddr" : "pmm" ), id);

    auto region_map = bpf.get_percpu_hash_table<u64, u64>(buf);
    return region_map;
}


static int get_perf_event_attr(const char *event_str, 
    struct perf_event_attr *pe) {
    int err;
    pfm_perf_encode_arg_t pfm;

    memset(pe, 0, sizeof(struct perf_event_attr));
    memset(&pfm, 0, sizeof(pfm_perf_encode_arg_t));

    /* perf_event_open */
    pe->size = sizeof(struct perf_event_attr);
    pe->exclude_kernel = 1;
    pe->exclude_hv     = 1;
    pe->precise_ip     = 2;
    pe->task           = 1;
    pe->use_clockid    = 1;
    pe->clockid        = CLOCK_MONOTONIC_RAW;
    pe->sample_period  = config.profile_overflow_thresh;
    pe->disabled       = 1;

    pe->sample_type  = PERF_SAMPLE_TID;
    pe->sample_type |= PERF_SAMPLE_TIME;
    pe->sample_type |= PERF_SAMPLE_ADDR;
    pe->sample_type |= PERF_SAMPLE_PHYS_ADDR;

    pfm.size = sizeof(pfm_perf_encode_arg_t);
    pfm.attr = pe;

    err = pfm_get_os_event_encoding(event_str, PFM_PLM2 | PFM_PLM3,
          PFM_OS_PERF_EVENT, &pfm);

    if (err != PFM_SUCCESS) {
        fprintf(stderr, "Couldn't find an appropriate event to use."
                "(error %d) Aborting.\n", err);
        return -EINVAL;
    }

    return 0;
}

int init_profiling(void) {
    struct timespec         ts;
    struct perf_event_attr  pe;
    int i;

    clock_gettime(CLOCK_REALTIME, &ts);
    start_timestamp_realtime = ts.tv_sec;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    start_timestamp_monotonic = ts.tv_sec;

    start_ms = measure_time_now_ms();

    auto res = bpf.init(BPF_PROGRAM);
    if (res.code() != 0) {
        std::cerr << res.msg() << std::endl;
        return 1;
    }

    auto control = bpf.get_array_table<int>("debug_control");
    control.update_value(0, 0);

    std::cout << "BPF program initialized\n" << std::flush << std::endl;

    pfm_initialize();
    if (get_perf_event_attr(DDR_MISS_EVENT_STR, &pe)) {
        std::cerr << "bad event: " << DDR_MISS_EVENT_STR << std::endl;
        return 1;
    }

    res = bpf.attach_perf_event_raw(&pe, "on_ddr_miss");
    if (res.code() != 0) {
      std::cerr << res.msg() << std::endl;
      return 1;
    }

    if (get_perf_event_attr(PMM_MISS_EVENT_STR, &pe)) {
        std::cerr << "bad event: " << PMM_MISS_EVENT_STR << std::endl;
        return 1;
    }

    res = bpf.attach_perf_event_raw(&pe, "on_pmm_miss");
    if (res.code() != 0) {
      std::cerr << res.msg() << std::endl;
      return 1;
    }

    if (config.nr_procs > MAX_PROCS) {
        std::cerr << "Error: too many procs requested: config.nr_procs: "
                  << config.nr_procs << " (max is " << MAX_PROCS << ")"
                  << std::endl;
        exit(-EINVAL);
    }

    for (i = (config.nr_procs-1); i >= 0; i--) {
        put_memprof_id(i);
    }

    std::cout << ":: Profiling ddr_miss and pmm_miss events on all processors.\n"
              << std::endl;
    return 0;
}


static std::vector<pid_t> get_procs_from_cgroup() {
    char                 path[256];
    char                 buf[64];
    FILE                *file;
    pid_t                pid;
    std::vector<pid_t>   procs;

    sprintf(path, "/sys/fs/cgroup/0/cgroup.procs");
    file = fopen(path, "r");
    if (file == NULL) {
        printf("could not open cgroup.procs\n");
        exit(-EINVAL);
    }

    while (fgets(buf, 64, file) != NULL) {
        pid = (pid_t) strtol(buf, NULL, 10);
        procs.push_back(pid);
    }
    fclose(file);

    return procs;
}


static void remove_dead_procs_from_memprof( std::vector<pid_t> &procs ) {
    // remove dead processes from proc_to_memprof_id
    for (auto it = proc_to_memprof_id.begin(); it != proc_to_memprof_id.end();) {
        if (std::find(procs.begin(), procs.end(), it->first) == procs.end()) {
            put_memprof_id(it->second);
            memprof_id_to_proc.erase(it->second);
            proc_to_memprof_id.erase(it++);
        } else {
            ++it;
        }
    }

    // remove dead processes from profile
    for (auto it = profile.begin(); it != profile.end();) {
        if (std::find(procs.begin(), procs.end(), it->first) == procs.end()) {
            it->second.clear();
            profile.erase(it++);
        } else {
            ++it;
        }
    }
}

static void add_new_procs_to_memprof( std::vector<pid_t> &procs ) {
    int memprof_id;

    // add new processes to the profile
    for (auto it = procs.begin(); it != procs.end(); it++) {
        if (proc_to_memprof_id.find(*it) == proc_to_memprof_id.end()) {
            memprof_id = take_memprof_id();
            proc_to_memprof_id[*it] = memprof_id;
            memprof_id_to_proc[memprof_id] = *it;
        }
    }

    // add new processes to the profile
    for (auto it = procs.begin(); it != procs.end(); it++) {
        if (profile.find(*it) == profile.end()) {
            profile[*it] = std::map<u64, region_info_t>();
        }
    }
}

static void remove_dead_procs_from_bpf( std::vector<pid_t> &procs ) {
    std::vector<u32>  bpf_procs_del_keys;
    std::vector<int>  ddr_misses_del_keys;
    std::vector<int>  pmm_misses_del_keys;

    auto bpf_procs = get_bpf_procs();
    auto ddr_misses = get_ddr_misses();
    auto pmm_misses = get_pmm_misses();

    auto offline = bpf_procs.get_table_offline();
    for (const auto &rec : offline) {
        if (std::find(procs.begin(), procs.end(), rec.first) == procs.end()) {
            bpf_procs_del_keys.push_back(rec.first);
            ddr_misses_del_keys.push_back(rec.second);
            pmm_misses_del_keys.push_back(rec.second);
        }
    }

    for (auto xkey : bpf_procs_del_keys) {
        bpf_procs.remove_value(xkey);
    }

    for (auto xkey : ddr_misses_del_keys) {
        auto region_map = get_region_map(DDR_MISS_EVENT, xkey);
        region_map.clear_table_non_atomic();
        ddr_misses.remove_value((u32)xkey);
    }

    for (auto xkey : pmm_misses_del_keys) {
        auto region_map = get_region_map(DDR_MISS_EVENT, xkey);
        region_map.clear_table_non_atomic();
        pmm_misses.remove_value((u32)xkey);
    }
}

static void add_new_procs_to_bpf ( std::vector<pid_t> &procs ) {
    int  memprof_id;

    for (auto it : procs) {

        if (proc_to_memprof_id.find(it) == proc_to_memprof_id.end()) {
            std::cerr << "Error: no memprof_id for proc! "
                      << "pid: " << it
                      << std::endl;
            exit(-ENOMEM);
        }

        auto bpf_procs = get_bpf_procs();
        auto ddr_misses = get_ddr_misses();
        auto pmm_misses = get_pmm_misses();

        auto res = bpf_procs.get_value(it, memprof_id);
        if ( res.code() == 0 ) {
            if (memprof_id != proc_to_memprof_id[it]) {
                std::cerr << "bpf procs out of sync with memprof!"
                          << " proc: " << it
                          << " memprof_id: " << proc_to_memprof_id[it]
                          << " bpf id: " << memprof_id
                          << std::endl;
                exit(-EINVAL);
            }
            continue;
        }

        memprof_id = proc_to_memprof_id[it];
        res = bpf_procs.update_value(it, memprof_id);
        if ( res.code() != 0 ) {
            std::cerr << "Error: could not insert proc ID: "
                      << "fd: " << bpf_procs.get_fd()
                      << "key: " << it
                      << std::endl;
            exit(-EINVAL);
        }

        auto ddr_region_map = get_region_map(DDR_MISS_EVENT, memprof_id);
        auto pmm_region_map = get_region_map(PMM_MISS_EVENT, memprof_id);

        std::cout << "New proc: " << it
                  << " memprof_id: " << memprof_id
                  << std::flush << std::endl;

        res = ddr_misses.update_value(memprof_id, ddr_region_map.get_fd());
        if ( res.code() != 0 ) {
            std::cerr << "Error: could not insert ddr_misses: "
                      << "ddr_misses_fd: " << ddr_misses.get_fd()
                      << "memprof_id: " << memprof_id
                      << "val: " << ddr_region_map.get_fd()
                      << std::endl;
            exit(-EINVAL);
        }

        res = pmm_misses.update_value(memprof_id, pmm_region_map.get_fd());
        if ( res.code() != 0 ) {
            std::cerr << "Error: could not insert pmm_misses: "
                      << "pmm_misses_fd: " << pmm_misses.get_fd()
                      << "memprof_id: " << memprof_id
                      << "val: " << pmm_region_map.get_fd()
                      << std::endl;
            exit(-EINVAL);
        }
    }
}


static void update_procs() {

    auto cur_procs = get_procs_from_cgroup();

    remove_dead_procs_from_memprof(cur_procs);
    add_new_procs_to_memprof(cur_procs);

    remove_dead_procs_from_bpf(cur_procs);
    add_new_procs_to_bpf(cur_procs);
}

/* region_info.llc_misses has the profile for the next interval (P_i+1).
 * It is computed as follows:
 *
 * P_i+1 = ((1 - alpha) * P_i) + (alpha * bpf_profile)
 *
 * alpha varies from [0,0, 1.0). Values closer to 1.0 weight accesses from
 * recent profiles more heavily, values closer to 0.0 have a 'stickier'
 * history
 */
static void add_bpf_region_map_to_profile( pid_t pid, enum llc_miss_event evt) {

    auto region_map = get_region_map( evt, proc_to_memprof_id[pid] );
    auto offline = region_map.get_table_offline();

    for (const auto &proc : profile) {
        for (auto &region : profile[proc.first]) {
            region.second.llc_misses_val[evt]     = ((u64)0);
            region.second.llc_misses_scaled[evt] *= (1.0 - config.alpha);
        }
    }

    for (const auto &rec : offline) {
        region_info_t &info = get_region_info(pid, rec.first);
        for (auto it : rec.second) {
            info.llc_misses_val[evt]    += it;
            info.llc_misses_tot[evt]    += it;
            info.llc_misses_scaled[evt] += (config.alpha*it);
        }
    }

    region_map.clear_table_non_atomic();
}

static void add_interval_to_profile() {

    for (auto proc_it = profile.begin();
              proc_it != profile.end(); proc_it++) {
        add_bpf_region_map_to_profile(proc_it->first, DDR_MISS_EVENT);
        add_bpf_region_map_to_profile(proc_it->first, PMM_MISS_EVENT);
    }
}


static void print_profile(int val, u64 cur_time) {
    pid_t pid;
    float ddr_misses_scaled, pmm_misses_scaled, llc_misses_scaled,
          cur_misses_scaled, cur_misses_ratio, cur_regions_ratio,
          reg_misses_scaled;
    u64   ddr_misses_tot, pmm_misses_tot, llc_misses_tot,
          ddr_misses_val, pmm_misses_val, llc_misses_val,
          total_regions, cur_regions;

    if (profile_print_cnt < config.print_profile_val) {
        profile_print_cnt += 1;
        return;
    }
    
    profile_file << std::dec
                 << "profile: " << std::setw(8) << val << "    "
                 << "time(ms): " << std::setw(12) << cur_time << "    "
                 << std::endl;

    for (auto proc_it : profile) {

        pid = proc_it.first;
        profile_file << std::dec << std::left
                  << "  PID: " << std::setw(8) << pid
                  << std::endl;

        auto region_infos = sorted_region_infos(proc_it.second);

        total_regions     = ((u64)0);
        ddr_misses_val    = pmm_misses_val    = ((u64)0);
        ddr_misses_tot    = pmm_misses_tot    = ((u64)0);
        ddr_misses_scaled = pmm_misses_scaled = 0.0;

        for (auto reg_it : region_infos) {
            ddr_misses_val    += reg_it.second.llc_misses_val[DDR_MISS_EVENT];
            pmm_misses_val    += reg_it.second.llc_misses_val[PMM_MISS_EVENT];

            ddr_misses_tot    += reg_it.second.llc_misses_tot[DDR_MISS_EVENT];
            pmm_misses_tot    += reg_it.second.llc_misses_tot[PMM_MISS_EVENT];

            ddr_misses_scaled += reg_it.second.llc_misses_scaled[DDR_MISS_EVENT];
            pmm_misses_scaled += reg_it.second.llc_misses_scaled[PMM_MISS_EVENT];

            total_regions += 1;
        }
        llc_misses_val    = (ddr_misses_val + pmm_misses_val);
        llc_misses_tot    = (ddr_misses_tot + pmm_misses_tot);
        llc_misses_scaled = (ddr_misses_scaled + pmm_misses_scaled);

        profile_file << "    regions: " << std::setw(12) << total_regions
                     << std::endl << std::right
                     << "    ddr_misses_scaled: " << std::setprecision(4)
                             << std::setw(13) << ddr_misses_scaled
                       << "  pmm_misses_scaled: " << std::setprecision(4)
                             << std::setw(13) << pmm_misses_scaled
                       << "  llc_misses_scaled: " << std::setprecision(4)
                             << std::setw(13) << llc_misses_scaled
                     << std::endl
                     << "    ddr_misses_val: " << std::setw(16)  << ddr_misses_val
                       << "  pmm_misses_val: " << std::setw(16)  << pmm_misses_val
                       << "  llc_misses_val: " << std::setw(16)  << llc_misses_val
                     << std::endl
                     << "    ddr_misses_tot: " << std::setw(16)  << ddr_misses_tot
                       << "  pmm_misses_tot: " << std::setw(16)  << pmm_misses_tot
                       << "  llc_misses_tot: " << std::setw(16)  << llc_misses_tot
                     << std::endl;

        if ( (total_regions == 0)   ||
             (llc_misses_tot == 0)  || 
             (config.print_region_limit < 0.0001) ||
             (config.print_misses_limit < 0.0001) ) {
            continue; 
        }

        cur_regions = ((u64)0);
        cur_misses_scaled = 0.0;
        cur_regions_ratio = cur_misses_ratio = 0.0;
        for (auto reg_it : region_infos) {
            if ( (cur_regions_ratio > config.print_region_limit) ||
                 (cur_misses_ratio  > config.print_misses_limit) ) {
                break;
            }

            ddr_misses_scaled = reg_it.second.llc_misses_scaled[DDR_MISS_EVENT];
            pmm_misses_scaled = reg_it.second.llc_misses_scaled[PMM_MISS_EVENT];
            reg_misses_scaled = (ddr_misses_scaled + pmm_misses_scaled);

            profile_file << "      ADDR: "
                      << std::left << std::hex << std::setw(16)
                      << (reg_it.first << MEMPROF_REGION_SHIFT)
                      << std::dec
                      << "DDR: " << std::setprecision(4)
                                 << std::setw(12) << ddr_misses_scaled
                      << "PMM: " << std::setprecision(4)
                                 << std::setw(12) << pmm_misses_scaled
                      << std::endl;

            cur_regions       += 1;
            cur_misses_scaled += reg_misses_scaled;
            cur_regions_ratio  = ((float)cur_regions / total_regions);
            cur_misses_ratio   = ((float)cur_misses_scaled  / llc_misses_scaled);
        }
        profile_file << std::endl;
    }
    profile_print_cnt = 1;
}

static void *profiling_thread(void *arg) {
    u64  start;
    u64  elapsed;
    int  profile_val;

#if 0
    // for debugging with bpf_trace_printk
    FILE *trace_pipe;
    int trace_fd, flags;
    char line[LINE_SIZE];

    trace_pipe = fopen("/sys/kernel/debug/tracing/trace_pipe", "r");
    trace_fd = fileno(trace_pipe);
    flags = fcntl(trace_fd, F_GETFL, 0); 
    flags |= O_NONBLOCK; 
    fcntl(trace_fd, F_SETFL, flags); 
#endif

    profile_val = profile_print_cnt = 0;
    start = measure_time_now_ms();
    update_procs();

    for (;;) {

        auto debug_control = bpf.get_array_table<int>("debug_control");
        debug_control.update_value(0, 10);

        elapsed = measure_time_now_ms() - start;
        if (elapsed < config.profile_period_ms) {
            usleep(1000 * (config.profile_period_ms - elapsed));
        } else {
            fprintf(stderr, "[!] profiling took longer than profile-period-ms\n");
        }

        start = measure_time_now_ms();

#if 0
        // for debugging with bpf_trace_printk
        while (fgets(line, LINE_SIZE, trace_pipe)) {
            std::cout << line << std::endl;
        }
#endif

        add_interval_to_profile();
        print_profile(profile_val, (start-start_ms));
        update_procs();

        profile_val += 1;
    }
}

int start_profiling_thread(void) {
    int status;
    status = pthread_create(&profiling_pthread, NULL, profiling_thread, NULL);
    if (status != 0) {
        fprintf(stderr, "failed to create profiling thread\n");
    }
    return status;
}
