#!/usr/bin/env bash

CC=/usr/lib/llvm-13/bin/clang++
C_FLAGS="-I /usr/include/bcc -Wall -Werror -O3 -g"
LD_FLAGS="-lm -lpfm -lpthread -lnuma -lbcc"

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "$script_dir"

pids=()

mkdir -p obj
rm -f obj/*
for f in src/*.cc; do
    echo "CC   ${f}"
    ${CC} -c ${C_FLAGS} -o obj/$(basename ${f} ".cc").o ${f} &
    pids+=($!)
done

for p in ${pids[@]}; do
    wait $p || exit 1
done

mkdir -p bin
rm -f bin/*
echo "LD   memprof"
${CC} ${LD_FLAGS} -o bin/memprof obj/*.o

echo "Done."
